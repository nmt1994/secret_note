import { Module } from '@nestjs/common';
import { NoteService } from './note.service';
import { NoteController } from './note.controller';
import { NoteServiceInterface } from './note.service.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './note.entity';

import { ConfigService } from '@nestjs/config';
const Cryptr = require('cryptr');

@Module({
  imports: [TypeOrmModule.forFeature([Note])],
  providers: [
    {
      provide: NoteServiceInterface,
      useClass: NoteService,
    },
    {
      provide: 'ENCRYPT_SERVICE',
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return new Cryptr(configService.get('NOTE_SECRET'));
      },
    },
  ],
  controllers: [NoteController],
})
export class NoteModule {}
