import { Note } from './note.entity';

export abstract class NoteServiceInterface {
  abstract getAllNotes(): Promise<Note[]>;
  abstract getNoteById(noteId: string, decrypt?: boolean): Promise<Note>;
  abstract createNote(noteContent: string): Promise<Note>;
  abstract updateNote(noteId: string, noteContent: string): Promise<void>;
  abstract deleteNote(noteId: string): Promise<void>;
}
