import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { QueryFailedError, EntityNotFoundError } from 'typeorm';

@Catch(QueryFailedError, EntityNotFoundError)
export class TypeOrmExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    let error = (exception as Error).stack;
    let message = 'Error from TypeORM: ';
    let status = HttpStatus.INTERNAL_SERVER_ERROR;

    switch (exception.constructor) {
      case QueryFailedError: {
        message += (exception as QueryFailedError).message;
        break;
      }
      case EntityNotFoundError: {
        status = HttpStatus.NOT_FOUND;
        message += (exception as EntityNotFoundError).message;
        break;
      }
      default:
        break;
    }

    response.status(status).json({ statusCode: status, message, error });
  }
}
