import { Test, TestingModule } from '@nestjs/testing';
import { NoteService } from './note.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { Repository } from 'typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
const Cryptr = require('cryptr');

describe('NoteService', () => {
  let service: NoteService;
  let testSetupRepo: Repository<Note>;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      // TODO: We could leverage test .env files
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
        }),
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (configService: ConfigService) => ({
            type: 'postgres',
            host: configService.get('DATABASE_HOST'),
            port: 5432,
            username: configService.get('DATABASE_USER'),
            password: configService.get('DATABASE_PASSWORD'),
            database: configService.get('DATABASE_NAME'),
            entities: [Note],
            synchronize: true,
            logging: true,
          }),
        }),
        TypeOrmModule.forFeature([Note]),
      ],
      providers: [
        NoteService,
        {
          provide: 'ENCRYPT_SERVICE',
          useFactory: () => {
            return new Cryptr('TEST_SECRET');
          },
        },
      ],
    }).compile();

    service = module.get<NoteService>(NoteService);
    testSetupRepo = service.getRepository();
  });

  afterEach(async () => {
    await testSetupRepo.clear();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be able to create/read a secret note', async () => {
    const noteContent = 'Hello';
    const createdNote = await service.createNote(noteContent);

    expect(createdNote.content).not.toBe(noteContent);

    const refetchedNote = await service.getNoteById(createdNote.id, true);
    expect(refetchedNote.content).toBe(noteContent);

    const noteList = await service.getAllNotes();
    expect(noteList.findIndex((i) => i.id === createdNote.id)).toBeGreaterThan(
      -1,
    );
    expect(noteList.length).toBe(1);
  });

  it('should be able to update a secret note', async () => {
    const currentContent = 'Hello';
    const newContent = 'Hello there';

    const createdNote = await service.createNote(currentContent);

    await service.updateNote(createdNote.id, newContent);

    const updatedNote = await service.getNoteById(createdNote.id, true);

    expect(updatedNote.content).toBe(newContent);
  });

  it('should be able to delete a secret note', async () => {
    const currentContent = 'Hello';
    const createdNote = await service.createNote(currentContent);

    await service.deleteNote(createdNote.id);
    expect(await service.getAllNotes()).toHaveLength(0);
  });

  it('should be able to fetch the note encrypted', async () => {
    const currentContent = 'Hello';
    const createdNote = await service.createNote(currentContent);

    const fetchedNote = await service.getNoteById(createdNote.id, false);
    expect(fetchedNote.content).not.toBe(currentContent);
  });
});
