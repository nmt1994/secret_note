import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Query,
  Post,
  Put,
} from '@nestjs/common';
import { NoteServiceInterface } from './note.service.interface';
import { UpsertNoteDto } from './dto/upsert-note';
import { FindNoteDto } from './dto/find-note';
import { Note } from './note.entity';

@Controller({
  version: '1',
})
export class NoteController {
  constructor(private readonly noteService: NoteServiceInterface) {}

  @Get()
  getNote(): Promise<Note[]> {
    return this.noteService.getAllNotes();
  }

  @Get('/:id')
  getSingleNote(
    @Param() params: FindNoteDto,
    @Query('decrypt') decrypt?: boolean,
  ): Promise<Note> {
    return this.noteService.getNoteById(params.id, decrypt);
  }

  @Post()
  @HttpCode(HttpStatus.ACCEPTED)
  createNote(@Body() upsertDto: UpsertNoteDto): Promise<any> {
    return this.noteService.createNote(upsertDto.noteContent);
  }

  @Put('/:id')
  updateNote(
    @Param() params: FindNoteDto,
    @Body() upsertDto: UpsertNoteDto,
  ): any {
    return this.noteService.updateNote(params.id, upsertDto.noteContent);
  }

  @Delete('/:id')
  deleteNote(@Param() params: FindNoteDto): any {
    return this.noteService.deleteNote(params.id);
  }
}
