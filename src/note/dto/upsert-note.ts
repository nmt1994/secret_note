import { IsNotEmpty } from 'class-validator';

export class UpsertNoteDto {
  @IsNotEmpty()
  noteContent: string;
}
