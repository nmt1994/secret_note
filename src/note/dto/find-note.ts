import { IsUUID } from 'class-validator';

export class FindNoteDto {
  @IsUUID()
  id: string;
}
