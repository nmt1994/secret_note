import { Inject, Injectable } from '@nestjs/common';
import { NoteServiceInterface } from './note.service.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { Repository } from 'typeorm';
import Cryptr from 'cryptr';

@Injectable()
export class NoteService implements NoteServiceInterface {
  constructor(
    @InjectRepository(Note) private readonly noteRepo: Repository<Note>,
    @Inject('ENCRYPT_SERVICE') private readonly cryptr: Cryptr,
  ) {}

  async getAllNotes(): Promise<Note[]> {
    return (await this.noteRepo.find()) ?? ([] as Note[]);
  }

  async getNoteById(noteId: string, decrypt?: boolean): Promise<Note> {
    const note = await this.noteRepo.findOneByOrFail({
      id: noteId,
    });

    if (decrypt === true) {
      note.content = this.cryptr.decrypt(note.content);
    }
    return note;
  }

  async createNote(noteContent: string): Promise<Note> {
    const newNote = this.noteRepo.create();
    newNote.content = this.cryptr.encrypt(noteContent);

    await this.noteRepo.insert(newNote);

    return newNote;
  }

  async updateNote(noteId: string, noteContent: string): Promise<any> {
    await this.noteRepo.findOneByOrFail({
      id: noteId,
    });

    return this.noteRepo.update(
      {
        id: noteId,
      },
      {
        content: this.cryptr.encrypt(noteContent),
      },
    );
  }

  async deleteNote(noteId: string): Promise<void> {
    await this.noteRepo.delete({
      id: noteId,
    });
  }

  public getRepository(): Repository<Note> {
    return this.noteRepo;
  }
}
