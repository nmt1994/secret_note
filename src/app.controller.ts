import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { HealthCheck, TypeOrmHealthIndicator } from '@nestjs/terminus';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly typeOrmHealthCheck: TypeOrmHealthIndicator,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/health')
  @HealthCheck()
  getHealth(): any {
    return this.typeOrmHealthCheck.pingCheck('database');
  }
}
